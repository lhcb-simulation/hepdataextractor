###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys,os

def extract_tables(sourcefile):
    """
    Simple function to extract tables from latex source file.
    Will save as the label of the table within the source code.
    Smart enough to try to resolve included files
    """
    full_path_to_sourcefile = os.path.dirname(sourcefile)
    tables_in_paper = {}
    ntables = 0
    if not os.path.isfile(sourcefile):
        print("File {} does not exist, skipping".format(sourcefile))
        return tables_in_paper

    with open(sourcefile) as f:
        lines = f.readlines()
        for i in range(len(lines)):
            if 'begin{table}' in lines[i] or 'begin{sidewaystable}' in lines[i]:
                # print('starting table')
                temptable = []
                j=0
                while not r"\end" in lines[i+j]:
                    if r"\label" in lines[i+j]:
                        temptablename = lines[i+j].split(r"\label{")[1].split("}")[0].replace("tab:","")
                    if not 'begin{' in lines[i+j]:
                        temptable.append(lines[i+j])
                    j+=1

                tables_in_paper[temptablename]=temptable
            if r'\input' in lines[i]:
                #skip commented inputs
                if "%" in lines[i]: continue
                extended_file = lines[i].split(r"\input{")[1].split("}")[0]+".tex"
                #pathological case where people also include .tex in input command
                if ".tex.tex" in extended_file:
                    extended_file = extended_file.replace(".tex.tex",".tex")
                path_to_included_file = os.path.join(full_path_to_sourcefile,extended_file)

                extended_dict = extract_tables(path_to_included_file)

                tables_in_paper = tables_in_paper | extended_dict
    return tables_in_paper


def main(args):
    print('now parsing',args.file)

    tables_in_paper = extract_tables(args.file)
    if False==args.no_output:
        print('now writing file for each of ',tables_in_paper.keys())
        for k,tabs in tables_in_paper.items():
            f = open(k+".tex",'w')
            for line in tabs:
                f.write(line)
            f.close()
    else:
        print('here is the output of the tables')
        print(tables_in_paper)
