###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

from .extract_tables_from_source import main as etfs
from .make_submission_yaml_file import main as msyf
from .tex_to_yaml import main as tty

def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="action", required=True)

    # extract_tables_from_source
    etfs_parser = subparsers.add_parser("extract_tables")
    etfs_parser.add_argument('-f','--file',type=str,required=True,help='input file to parse')
    etfs_parser.add_argument('--no_output',action="store_true",help='skip output')
    etfs_parser.set_defaults(func=etfs)

    # tex_to_yaml
    tty_parser = subparsers.add_parser("tex_to_yaml")
    tty_parser.add_argument("--file", type=str,required=True,help='file contains name of data file')
    tty_parser.add_argument("--format", type=str, required=False,help='''format contains string of data format (not necessary for covariance)
     A guide: i -- independent variable,
      d -- dependent variable,
      stat -- statistical error,
      sys -- systematic error''')
    tty_parser.add_argument("--covariance", action="store_true", default=False,help='Flag to say whether the data is a covariance matrix or not') #
    tty_parser.add_argument("--correlation", action="store_true", default=False,help='Same as covariance, but for a correlation matrix') #
    tty_parser.add_argument("--qualifiers", type=str, default="None",help='qualifiers contains name of qualifiers file, defaults to "None"')
    tty_parser.add_argument("--remove", nargs='+', help="list of terms to remove from table")
    tty_parser.set_defaults(func=tty)

    # make_submission_yaml_file
    msyf_parser = subparsers.add_parser("prepare_submission",
        description="Preparation of submission.yaml file for HepData submission. "
        "Requires the data yaml files produced from tex_to_yaml to be provided")
    msyf_parser.add_argument('-f','--files',type=str,nargs='+',required=True,help='input yaml file(s) to parse. Note, can pass *.yaml')
    msyf_parser.set_defaults(func=msyf)

    args = parser.parse_args()
    args.func(args)
