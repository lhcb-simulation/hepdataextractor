###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys,os
import argparse

from collections import OrderedDict

from string import Template

import yaml



def get_metadata_info_for_yaml(input_yaml):
    """
    Simple wrapper to get the metadata information necessary for the submission.yaml file.
    Uses safe_substitute on the template in the Templates directory to ensure return even
    in the case of error.
    """
    conf = yaml.safe_load(open(input_yaml,'r'))
    # print('got yaml with keys',conf.keys())
    #find the description
    with open(input_yaml.replace('.yaml','_description_file.txt'),'r') as descf:
        description_for_yaml = "".join(descf.readlines()).replace("description: ","")
        # print('got description {}'.format(description_for_yaml))
    the_dict = {'observables':"",
                'independent_vars':"",
                'description':description_for_yaml,
                'datafile':input_yaml,
                'special':""}
    observables = []
    independent_vars =[]
    for thing in conf['dependent_variables']:
        # print('appending',thing['header']['name'])
        if 'Covariance' in thing['header']['name'] or 'Correlation' in thing['header']['name']:
            # print('skipping observables addition for correlation/covariance')
            the_dict['special']=str(thing['header']['name'])
        else:
            observables.append(thing['header']['name'])
    for thing in conf['independent_variables']:
        # print('now appending independent variables',thing['header']['name'])
        independent_vars.append(thing['header']['name'])
    # print(the_dict)
    if ''==the_dict['special']:
        # the_dict['description'] = ', '.join(observables) + ' as a function of '+", ".join(independent_vars)
        the_dict['observables'] = '"'+'", "'.join(observables)+'"'
    else:
        vars_sorted = list(OrderedDict.fromkeys(independent_vars))
        # the_dict['description'] = the_dict['special'] + ' for ' +', '.join(vars_sorted)

    the_template = Template(open(os.path.dirname(os.path.abspath(__file__))+"/Templates/submission_template.yaml",'r').read())

    return the_template.safe_substitute(the_dict)


def main(args):
    for f in args.files:
        if not '.yaml' in f:
            print("You haven't passed valid yaml files! Please try again")
            sys.exit(1)
    for f in args.files:
        if 'submission.yaml' in f:
            print('will overwrite submission.yaml. removing file from list to be used')
            args.files.remove('submission.yaml')

    #search for file containing the relevant description for the submission.yaml file
    for f in args.files:
        if not os.path.isfile(f.replace('.yaml','_description_file.txt')):
             print('missing description file for {}. Consider re-running the tex_to_yaml conversion!'.format(f))
             sys.exit(1)
    file_metadata = []
    print('Processing yaml files: {}'.format(args.files))
    for f in args.files:
        file_metadata.append(get_metadata_info_for_yaml(f))

    submission_file = open("submission.yaml",'w')
    submission_file.write("# Start of submission. Extra metadata\n")
    submission_file.write("# additional_resources: # additional references, e.g. LHCb public site\n")
    submission_file.write('# - {location: "https://lhcbproject.web.cern.ch/Publications/LHCbProjectPublic/LHCb-PAPER..."}\n')
    submission_file.write('# comment: | # Information that applies to all data tables. Can write a brief paragraph here\n')

    submission_file.write("---\n")
    submission_file.write("# Start of table entries.\n")
    for f in file_metadata:
        submission_file.write(f)
        submission_file.write('\n')
        submission_file.write("---\n")
        submission_file.write('\n')


    submission_file.close()
    print('Done!')
