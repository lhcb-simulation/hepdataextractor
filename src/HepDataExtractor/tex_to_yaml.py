###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import numpy as np
import re

class uncertainty:
    """
    Uncertainty class to contain the magnitude and type
    """
    def __init__(self, unc, unc_type):
        self.unc = unc
        self.unc_type = unc_type
    def __repr__(self):
        if self.unc_type == "asym": #Asymmetric error denoted by "asym", list of form [plus_error, minus_error]
            return "    - {asymerror: {plus: " + str(self.unc[0]) + ", minus: -" + str(self.unc[1]) + "}}\n"
        elif self.unc.strip(" ") == "":
            return ""
        elif self.unc_type == "e": #Normal symmetric uncertainty denoted by "e"
            return "    - {symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "stat": #Statistical uncertainty denoted by "stat"
            return "    - {label: stat, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "sys": #Systematic uncertainty denoted by "sys"
            return "    - {label: sys, symerror: " + str(self.unc) + "}\n"
        elif self.unc_type == "lumi": #Luminosity uncertainty denoted by "lumi"
            return "    - {label: 'sys,lumi', symerror: " + str(self.unc) + "}\n"
        else: #Also allows for custom uncertainty label
            return "    - {label: '" + self.unc_type + "', symerror: " + str(self.unc) + "}\n"

class value:
    """
    Value class to contain a value and a list of its uncertainties
    """
    def __init__(self, x):
        self.x = x
        self.uncertainties = []
        self.range = None
    def append(self, unc): #Overwrites append to add an uncertainty object to the list
        self.uncertainties.append(unc)
    def refine_range(self,delimiter = '--'):
        """
        if the format of the string contains --, it should be a range. overwrite the "x" value
        """
        if self.x.find(delimiter)!= -1:
            vals  =self.x.split("--")
            lowval = min(vals)
            highval = max(vals)
            retstring = "{"+" low: {lowval}, high: {highval} ".format(lowval=lowval,highval=highval)+"}"
            self.range = retstring
            return
        else:
            return
    def __repr__(self):
        if len(self.uncertainties) > 0:
            output =  "  - errors:\n"
            for i in self.uncertainties:
                output += repr(i)
            output += "    value: " + str(self.x) + "\n"
        elif not None==self.range:
            output ="  - "+self.range+"\n"
        else:
            output = "  - value: " + str(self.x) + "\n"
        return output

class variable:
    """
    Variable class to contain the column header and each value the variable takes, with uncertainties
    """
    def __init__(self, is_indep, header, qualifiers):
        self.is_indep = is_indep
        self.header = header
        self.qualifiers = qualifiers #qualifiers should be the name of a file
        self.values = []
    def append(self, val): #Overwrites apppend to add value objects to values
        self.values.append(val)
    def __repr__(self):
        output = "- header: {" + self.header + "}\n"
        if self.qualifiers != "None":
            output += "  "
            qualifiers_file = open(self.qualifiers, "r")
            output += qualifiers_file.read()
            output += "\n"
        output += "  values:\n"
        for i in self.values:
            output += repr(i)
        return output

def discard(line):
    """
    Discards lines in tex file that begin with % or \\, but not if it begins with \\multiline
    """
    line = line.strip(" \n\t~{}")
    if line == "":
        return True
    elif line.startswith(r"\multicolumn"):
        return False
    elif line[0] == "%" or line[0] == "\\":
        return True
    else:
        return False

def extract_header(string):
    """
    Extracts the name and units (if any) from the header of a column and returns a string suitable for yaml
    """
    string = string.replace(r"\xspace", "").replace(r"\kern", "").replace(r"\,", "").replace(r"\!", "").replace("-0.1em", "").strip(" \n\t~\\")
    if string.startswith("multicolumn"): #Removes leading multicolumn expression from header
        string = string[string.find("}")+1:]
        string = string[string.find("}")+1:]
    string.strip("{}")

    if "[" in string and "]" in string: #Takes expression in square brackets to be the units
        units = string[string.find("[")+1:string.find("]")].strip(" \n\t~\\")
        string = string[:string.find("[")].strip(" \n\t~\\")
        if units.startswith("$") and units.endswith("$"):
            return "name: '" + string + "', units: '" + units + "'"
        else:
            return "name: '" + string + "', units: " + units
    else:
        return "name: '" + string.strip(" \n\t~\\") + "'"

def extract_description(string):
    """
    Extract the description field from the caption listed in paper
    """
    temp_string = string.replace("\\n","").replace("\\xspace","").replace("\\","").rstrip().strip()

    if 'label' in temp_string:
        to_rep = re.findall(r"label{[tab:]*[a-zA-Z_]*}",temp_string)
        for rep in to_rep:
            temp_string = temp_string.replace(rep,"")
    temp_string = temp_string.replace('caption{',"").replace("}","")
    return temp_string


def split_format(string):
    """
    Splits format string so that each variable is its own string in a list
    """
    temp_list = []
    for i in string.split(" "):
        if i == "i" or i == "d":
            temp_list.append([i])
        else:
            temp_list[-1].append(i)
    return temp_list

def format_value(string):
    """
    Takes string with errors separated by commas and returns value object
    """
    string = string.strip(" \n\t~\\!$")
    if string == "":
        return value("'-'")
    else:
        if "[" in string and "]" in string and "," in string:
            the_val = value(string.replace("[", "").replace("]", "").replace(",", "--"))
            the_val.refine_range()
            return the_val
        else:
            the_val = value(string)
            the_val.refine_range()
            return the_val


def write_variables(variable_list, file):
    """
    Writes variable objects in list to a yaml file
    """
    file.write("independent_variables:\n")
    for i in variable_list:
        if i.is_indep == True:
            file.write(repr(i))
    file.write("dependent_variables:\n")
    for i in variable_list:
        if i.is_indep == False:
            file.write(repr(i))


def write_description(description,file):
    """
    Writes the description to a yaml file
    """
    file.write("description: {} \n".format(description))

def remove_phantoms(string): #
    """
    Removes \\phantom{...} from string
    """
    while r"\phantom" in string:
        x = string.find(r"\phantom")
        while string[x] != "}":
            x += 1
        string = string[:string.find(r"\phantom")] + string[x+1:]
    return string

def asym_unc(string):
    """
    Takes a string of the form value ^ +pos_error _ -neg_error and returns an asymmetric uncertainty object
    """
    string = string.replace(" ", "").replace("^", "").replace("_", "").replace("{", "").replace("}", "")
    return uncertainty([string[string.find("+")+1:string.find("-")], string[string.find("-")+1:]], "asym")



def main(args):
    description = None
    if args.covariance==False and args.correlation == False and args.format ==None:
        print('Must pass format if not passing a correlation or covariance matrix!')
        exit(1)
    input_file = open(args.file, "r")
    lines = input_file.readlines() #Stores each line of the file as a string in in the lines list
    input_file.close()

    if not args.remove is None:
        remove_list = args.remove.split(" ")
        for item in remove_list:
            for i in range(len(lines)):
                if item in lines[i]:
                    lines[i] = lines[i].replace(item, "")

    data_array = [] #2D array to store data content
    for i in lines:
        if discard(i) == False: #Only valid lines
            if args.covariance == True or args.correlation == True:
                data_array.append(remove_phantoms(i).replace(r"\hline", "").strip(" \n\t~\\").split("&")) #Splits into array by &
            else:
                if len(data_array) == 0: #Does not take convert the first line to values, as this contains headers
                    data_array.append(remove_phantoms(i).replace(r"\hline", "").strip(" \n\t~\\").split("&"))
                else:
                    line_split = remove_phantoms(i).replace(r"\hline", "").strip(" \n\t~\\$").split("&")
                    if r"\pm" in line_split: #Removes items that are just \pm
                        line_split.remove(r"\pm")
                    if r" \pm " in line_split:
                        line_split.remove(r" \pm ")
                    count = 0 #Count iterates in the format string to determine whether each number is a indep value, dep value, sys uncertainty, etc.
                    value_list = [] #List of values extracted from this line
                    for j in line_split: #For item in line split by "&"
                        temp = j.split(r"\pm")
                        temp = [x.strip(" \n\t~\\$") for x in temp]
                        if len(temp) > 1: #Removes blank characters from temp
                            if "" in temp:
                                temp.remove("")
                            if " " in temp:
                                temp.remove(" ")
                        if args.format.split(" ")[count] == "i" or args.format.split(" ")[count] == "d": #Creates new value if i or d
                            value_list.append(format_value(temp[0].strip(" \n\t~\\$!")))
                            count += 1
                            for k in range(1, len(temp)): #Appends errors if there are in the same string in line_split
                                if args.format.split(" ")[count] == "asym":
                                    value_list[-1].append(asym_unc(temp[k].strip(" \n\t~\\$!")))
                                else:
                                    value_list[-1].append(uncertainty(temp[k].strip(" \n\t~\\$!"), args.format.split(" ")[count]))
                                count += 1
                        else: #Appends errors if they are in a new string in line_split
                            if args.format.split(" ")[count] == "asym":
                                value_list[-1].append(asym_unc(temp[0].strip(" \n\t~\\$!")))
                            else:
                                value_list[-1].append(uncertainty(temp[0].strip(" \n\t~\\$!"), args.format.split(" ")[count]))
                            count += 1
                    data_array.append(value_list) #Appends values from this line to 2D array
        elif discard(i)== True and 'caption' in i:
            #add a description tag from the caption
            description = extract_description(i)
    data_array = np.stack(data_array, axis=0) #Converts to 2D numpy array

    variables = [] #Creates list for variable objects to eb stored
    if args.covariance == True or args.correlation == True:
        for i in range(len(data_array)):
            for j in range(len(data_array[0])):
                if data_array[i,j].strip(" \t-") == "":
                    #Fills blank spaces with diagonally opposite entries to create a diagonally symmetric matrix from a triangle matrix
                    data_array[i,j] = data_array[j,i]
        indep_header = extract_header(data_array[0,0])
        if indep_header == "name: ''":
            #If the data has no header in the top-left, and the row/column labels have subscripts, the subscripts become the headers
            if "_" in data_array[0,1]:
                variables.append(variable(True, "name: '$" + data_array[0,1][data_array[0,1].find("_")+1:].replace("$", "").strip(" ") + "$'", "None"))
            else:
                variables.append(variable(True, indep_header, "None"))
            if "_" in data_array[1,0]:
                variables.append(variable(True, "name: '$" + data_array[1,0][data_array[1,0].find("_")+1:].replace("$", "").strip(" ") + "$'", "None"))
            else:
                variables.append(variable(True, indep_header, "None"))
        else:
            variables.append(variable(True, indep_header, "None"))
            variables.append(variable(True, indep_header, "None"))
        if args.covariance == True:
            variables.append(variable(False, "name: Covariance", args.qualifiers))
        else:
            variables.append(variable(False, "name: Correlation", args.qualifiers))
        x_axis = [] #Contains bin labels from x-axis
        y_axis = [] #Contains bin labels from y-axis
        for i in data_array[0, 1:]:
            #Appends bin labels to x-axis (only up to the start of the subscript if there is one)
            if "_" in i:
                x_axis.append(format_value(i[:i.find("_")]))
            else:
                x_axis.append(format_value(i))
        for i in data_array[1:, 0]:
            #Appends bin labels to y-axis (only up to the start of the subscript if there is one)
            if "_" in i:
                y_axis.append(format_value(i[:i.find("_")]))
            else:
                y_axis.append(format_value(i))
        for i in range(len(x_axis)):
            for j in range(len(y_axis)):
                #Appends bin labels in repeating interlacing pattern suitable for a correlation matrix
                # print('appending {} --> {} --> '.format(x_axis[j], type(x_axis[j].x), x_axis[j].refine_range()))
                x_axis[j].refine_range()
                y_axis[i].refine_range()
                variables[0].append(x_axis[j])
                variables[1].append(y_axis[i])

        dep_values = data_array[1:,1:].flatten()
        for i in dep_values:
            #Appends correlation to the dependent variable
            variables[2].append(format_value(i))

    else:
        #If not a covariance matrix
        format_list = split_format(args.format)
        #Iterates along the format
        for i in range(len(format_list)):
            #Creates variable objects where needed and stores in variables
            if format_list[i][0] == "i":
                variables.append(variable(True, extract_header(data_array[0,i]), "None"))
            elif format_list[i][0] == "d":
                variables.append(variable(False, extract_header(data_array[0,i]), args.qualifiers))
        for i in range(1,len(data_array)):
            for j in range(len(data_array[i,:])):
                variables[j].append(data_array[i,j])

    output_file = open(args.file.replace(".tex", ".yaml"), "w") #Creates file with same name, but .yaml instead of .tex

    write_variables(variables, output_file) #Writes contents of variables list to file
    output_file.close()
    #write extra file for description

    if description is not None:
        output_description_file = open(args.file.replace('.tex','_description_file.txt'),'w')
        write_description(description, output_description_file)
        output_description_file.close()
