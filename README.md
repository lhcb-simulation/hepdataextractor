# HepDataExtractor

Simple python tool to extract tables from LaTeX papers and convert them to HepData entries


## Helping to Develop

Spotted an error? Need something special you think is useable by others? Create either an Issue or a Merge Request.


## Getting started

### I have a LaTeX file with just the tables I want to process

In some ways this is the easiest way to do things! You can simply pass the file to `tex_to_yaml` via the command line, e.g.

```
gethepdata tex_to_yaml --file myfile.tex --format "i d stat syst d stat syst"
```

if you need help with the command line documents, type

```
gethepdata tex_to_yaml --help
```


### I have a document which I want to process the tables for

Not a problem! The command `extract_tables` extracts all the tables from a document (including recursively by passing only the `main.tex` file). You can call from the command line using

```
gethepdata extract_tables --file /path/to/main.tex
```
which will then use the `\label` argument to determine the `.tex` file name.

### My submission is ready! Create the `submission.yaml` file

Once you have all tables in the `.yaml` format, you can run the command `prepare_submission` to create the relevant file suitable for HepData submission

```
gethepdata prepare_submission --files *.yaml
```

This will create the `submission.yaml` based on the output of the `tex_to_yaml` step.


Original Author: Harvey Turner, Manchester, supervised by Adam Davis and Will Barter. Updated 11 August 2023 by Adam Davis
