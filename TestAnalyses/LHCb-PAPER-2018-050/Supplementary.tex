\section{Supplemental material}
\subsection{\boldmath Relationships between raw $H_c\mu^-X$ measured yields and corrected yields}

The corrected yields for $\Bzb$ or $B^-$ mesons decaying into $D^0\munu X$ or $D^+\munu X$, $n_{\rm corr}$, can be expressed in terms of the measured yields, $n$, as

\begin{align}
\label{eq:dzero}
n_{\rm corr}(B\to \Dz \mun)&=\frac{1}
{{\cal{B}}(\Dz\to \Km\pip)\epsilon(B \to \Dz)}\times \\\nonumber 
&\left[n(\Dz\mu^-)-n(\Dz\Kp\mun)
\frac{\epsilon(\Bsb \to \Dz)}{\epsilon(\Bsb \to \Dz\Kp)}-n(\Dz p\mun)
\frac{\epsilon(\Lb \to \Dz)}{\epsilon(\Lb \to \Dz p)}\right],
\end{align}
where we use the  shorthand $n(D\mun)\equiv n(DX\munu)$.  An analogous abbreviation $\epsilon$ is used for the total trigger and detection efficiencies.  
For example, the ratio \mbox{$\epsilon(\Bsb \to D^0K^+)/\epsilon(\Bsb \to D^0)$} gives the relative efficiency to reconstruct a charged kaon in semimuonic $\Bsb$ decays producing a $\Dz$ meson. The second term in this equation accounts for the $\Dz\mun$ pairs originating from a $\Bsb$ decay, such as $\Bsb\to\Dz\Kp\mun$, while the third term accounts for the $\Dz\mun$  pairs originating from $\Lb$ semileptonic decays. These components are determined from the study of the final states  $\Dz\Kp\mun$ and  $\Dz p\mun$respectively. The branching fraction ${\cal B}(\Dz\to\Km\pip)$ appears because this decay mode is used in this study.
Similarly
\begin{eqnarray}
n_{\rm corr}(B\to \Dp \mun)&=&\frac{1}{\epsilon(B \to \Dp)}\left[
\frac{n(\Dp\mun)}{{\cal{B}}(\Dp\to \Km\pip\pip)} \right. \nonumber \\  
&&\left. -\frac{n(\Dz\Kp\mun)}{{\cal{B}}(\Dz\to \Km\pip)}
\frac{\epsilon(\Bsb \to \Dp)}{\epsilon(\Bsb \to \Dz\Kp)}  \right. \nonumber \\
&&\left.
-\frac{n(\Dz p\mun)}{{\cal{B}}(\Dz\to \Km\pip)}\frac{\epsilon(\Lb \to \Dp)}{\epsilon(\Lb \to \Dz p)}\right].\label{eq:dp}
\end{eqnarray}
Both the $\Dz X\munu$ and the $\Dp X \munu$ final states contain small components of cross-feed from $\Bsb$ decays to $\Dz K^+ X\munu$ and to $D^+ \Kz X\munu$, and from $\Lb$ decays to $\Dz p X\munu$ and  to $D^+ n X\munu$. Here we use isospin symmetry and infer the contributions by $\Dp\mun$ pairs originating from a $\Bsb$ decay, such as $\Bsb\to\Dp\Kz\mun\neumb$ from the $\Dz\Kp\mun$  final states, and the contributions from $\Lb\to\Dp n\mun\neumb$ from the $ \Dz p\mun$ yields.


The number of $\Bsb\to \Ds X\munu$ decays in the final state is given by
\begin{eqnarray}
% \label{eq:bsds}
n_{\rm corr}(\Bsb\to\Ds \mu^-)&=& \frac{n(\Ds\mu^-)}{{\cal B}(\Ds\to K^+K^-\pi^+)\epsilon(\Bsb\to\Ds\mu^-)}  \nonumber \\  \label{eq:bsds}
& &-N(\Bzb+B^-){\cal B}(B\to \Ds \Kb^0)\frac{\epsilon(\Bb\to\Ds \Kb^0\mu^-)}{\epsilon(\Bsb\to\Ds \mu^-)}.
\end{eqnarray}
In addition, the $\Bsb$ meson decays semileptonically into $DKX\munu$, and thus we need to add to Eq.~\ref{eq:bsds} the term
\begin{equation}
n_{\rm corr}(\Bsb\to DK\mu^-)=\kappa \frac{n(\Dz K^+\mu^-)}{{\cal B}(\Dz\to K^-\pi^+)\epsilon(\Bsb\to D^0K^+\mu^-)}, \label{eq:bsdk}
\end{equation}
where $\kappa$ accounts for the unmeasured $\Bsb\to D^+KX\munu$ semileptonic decays. The correction $\kappa$ is evaluated using the known decay modes of the $D_{s1}(2536)^+$ and $D^*_{s2}(2573)^+$  states and assuming that the nonresonant component of the hadronic mass spectrum decays in equal portions into $D$ or $\Dstar$ final states. The last term in Eq.~\ref{eq:bsds} accounts for $\Ds K X\munu$ final states originating from $\Bzb$ or $B^-$ semileptonic decays, and $N(\Bzb+B^-)$ indicates the total number of $\Bzb$ and $B^-$ produced. We derive this correction using the PDG value for the branching fraction ${\cal B}(B^-\to D_s^{(*)+}K^-\mu^-\nu)=(6.1\pm 1.0)\times 10^{-4}$, and assuming the same rate for \Bsb decays using isospin invariance \cite{Tanabashi:2018oca}.  



The equation for the ratio $\rbs$ is 
\begin{eqnarray}
\label{eq:frac-final}
\frac{\fs}{\fu+\fd}=\frac{n_{\rm corr}(\Bsb\to D\mu^-)}{n_{\rm corr}(B\to D^0 \mu^-)+n_{\rm corr}(B\to D^+ \mu^-)}
\frac{\tau_{B^-}+\tau_{\Bzb}}{2\tau_{\Bsb}}(1-\xi_{s})  \nonumber \\ 
- \frac{\mathcal{B}(B\rightarrow D_{s}\Kb\mu^-)}{\left<\mathcal{B}_{\rm SL}\right >} \frac{\epsilon(\Bb\rightarrow D_{s}^{+})}{\epsilon(\Bsb\rightarrow D_{s}^{+})} ,
\end{eqnarray}
where $\Bsb\to D\mu$ represents  $\Bsb$ semileptonic decays to a charmed hadron, given by the sum of the contributions shown in Eqs. \ref{eq:bsds} and \ref{eq:bsdk}, and the symbols $\tau _{B_i}$ indicate the $B_i$ hadron lifetimes, that are all well measured~\cite{Tanabashi:2018oca}.  We use the average $\Bsb$ lifetime, $1.526\pm0.015$~ps. This equation assumes equality of the semileptonic widths of all the $b$-hadron species. This is a reliable assumption, as corrections in HQET arise only to order 1/$m_b^2$ and the SU(3) breaking correction is quite small, $(-1.0\pm0.5)\%$ \cite{Bigi:2011gf}. The parameter $\xi_s$ accounts for this small adjustment. The second term is the subtraction of the $B^{-,0}\rightarrow \Ds \Kb X\munu$ component that is reconstructed in the signal sample as described in Eq.~\ref{eq:bsds}. The $\mathcal{B}_{\rm SL}$ term in the denominator is the semileptonic branching fraction of the $\Bsb$ derived using the equality of the semileptonic widths and the measured lifetime of the $\Bsb$, listed in Table~\ref{tab:slpar}.

The $\Lb$ corrected yield is derived in an analogous manner
\begin{equation}
n_{\rm corr}(\Lb\to H_c\mu^-)=\frac{n(\Lc\mun)}{{\cal{B}}(\Lc\to p\Km\pip)\epsilon(\Lb \to \Lc)}
+2\frac{n(\Dz p\mun)}{{\cal{B}}(\Dz\to \Km\pip)\epsilon(\Lb \to \Dz p)},\label{eq:lb}
\end{equation}
where $H_c$ represents a generic charmed hadron. The second term includes the cross-feed channel and the factor of two accounts for the isospin $\Lb \rightarrow D^{+} n\mun$ decay. The $\Lb$ fraction is written as
\begin{equation}
\label{eq:frac-final-lb}
\frac{f_{\Lb}}{\fu+\fd}= \frac{n_{\rm corr}(\Lb\to H_c \mu^-)}{n_{\rm corr}(B\to \Dz \mu^-)+n_{\rm corr}(B\to \Dp \mu^-)}
\frac{\tau_{\Bm}+\tau_{\Bzb}}{2\tau_{\Lb}} (1-\xi_{\Lb}).
\end{equation}
While  we assume near equality of the semileptonic widths of different $b$ hadrons, we apply a small adjustment  $\xi_{\Lb}=(3.0\pm1.5)$\%, to account for the chromomagnetic correction, affecting $b$-flavored mesons but not $b$ baryons~\cite{Bigi:2011gf}. The uncertainty is evaluated with conservative assumptions for all the parameters of the heavy quark expansion.

\newpage
\subsection{\boldmath Table of $b$-fractions versus $\pt(H_b)$}

\begin{table}[h]
	\caption{Values of $f_s/(f_u+f_d)$ and  $f_{\Lb}/(f_u+f_d)$ in each $\pt(H_b)$ bin. The first uncertainty is statistical and incorporates both the uncertainties due to the data sample size and the finite amount of simulated events, while the second is the overall systematic uncertainty, including global and bin-dependent systematic uncertainties.
	 \label{tab:ptB}}
	\centering
	\begin{tabular}{rccc}\hline\hline
	$\pt(H_b)[\!\gev]$ & $f_s/(f_u+f_d)$ &   $f_{\Lb}/(f_u+f_d)$  \\[0.1cm]
	\hline	
4--5   ~~   & $0.125 \pm 0.001 \pm 0.007$ &  $0.324\pm 0.001\pm 0.025$ \\
5--6    ~~  & $0.125\pm 0.001 \pm 0.007$ &  $0.281\pm 0.001\pm 0.018$ \\
6--7    ~~  & $0.122 \pm 0.001 \pm 0.006$ &  $0.257\pm 0.001\pm 0.017$ \\	
7--8    ~~  & $0.125\pm 0.001 \pm 0.006$ &  $0.245\pm 0.001\pm 0.017$ \\
8--9   ~~  & $0.116 \pm 0.001 \pm 0.006 $&  $0.227\pm 0.001\pm 0.015$ \\
9--10 ~~ & $0.120 \pm 0.001 \pm 0.006$&  $0.210\pm 0.001\pm 0.015$ \\
10--11~~~ & $0.121 \pm 0.001 \pm 0.006 $&  $0.194\pm 0.001\pm 0.013$ \\
11--12  ~~~& $0.116 \pm 0.001 \pm 0.006 $&  $0.191\pm 0.001\pm 0.014$ \\
12--13 ~~ & $0.116 \pm 0.001 \pm 0.006 $&  $0.172\pm 0.001\pm 0.013$ \\
13--14 ~~ & $0.122 \pm 0.001 \pm 0.007 $&  $0.159\pm 0.001\pm 0.012$ \\
14--16 ~~ & $0.112 \pm 0.001 \pm 0.006 $&  $0.165\pm 0.001\pm 0.012$ \\
16--18 ~~ & $0.107 \pm 0.001 \pm 0.006 $&  $0.136\pm 0.001\pm 0.010$ \\
18--20 ~~ & $0.115 \pm 0.001 \pm 0.008 $&  $0.126\pm 0.001\pm 0.010$ \\
20--25 ~~ & $0.111 \pm 0.001 \pm 0.007 $&  $0.109\pm 0.001\pm 0.009$ \\		
%4--5& 2.350E-
%4--5   ~~   & $0.125 \pm 0.001 \pm 0.007$ &  $0.324\pm 0.001\pm 0.025$ \\
%5--6    ~~  & $0.125 \pm 0.001 \pm 0.007$ &  $0.281\pm 0.001\pm 0.018$ \\
%6--7    ~~  & $0.122 \pm 0.001 \pm 0.006$ &  $0.257\pm 0.001\pm 0.017$ \\	
%7--8    ~~  & $0.125 \pm 0.001 \pm 0.006$ &  $0.245\pm 0.001\pm 0.017$ \\
%8--9   ~~  & $0.116 \pm 0.001 \pm 0.006 $&  $0.227\pm 0.001\pm 0.015$ \\
%9--10 ~~ & $0.120 \pm 0.001 \pm 0.006$&  $0.210\pm 0.001\pm 0.015$ \\
%10--11~~~ & $0.121 \pm 0.001 \pm 0.007 $&  $0.194\pm 0.001\pm 0.013$ \\
%11--12  ~~~& $0.116 \pm 0.001 \pm 0.006 $&  $0.191\pm 0.001\pm 0.014$ \\
%12--13 ~~ & $0.116 \pm 0.001 \pm 0.007 $&  $0.172\pm 0.001\pm 0.013$ \\
%13--14 ~~ & $0.122 \pm 0.001 \pm 0.007 $&  $0.159\pm 0.001\pm 0.012$ \\
%14--16 ~~ & $0.112 \pm 0.001 \pm 0.006 $&  $0.165\pm 0.001\pm 0.012$ \\
%16--18 ~~ & $0.107 \pm 0.001 \pm 0.007 $&  $0.136\pm 0.001\pm 0.010$ \\
%18--20 ~~ & $0.115 \pm 0.001 \pm 0.008 $&  $0.126\pm 0.001\pm 0.010$ \\
%20--25 ~~ & $0.111 \pm 0.001 \pm 0.007 $&  $0.109\pm 0.001\pm 0.009$ \\	
	\hline\hline
	\end{tabular}
\end{table}
%\clearpage

\subsection{\boldmath Fraction ratios as functions of $\eta$}

Figure~\ref{BFraction_eta} shows measurements of the fraction ratios $f_s/(f_u+f_d)$ and $f_{\Lb}/(f_u+f_d)$ as functions of $\eta$, integrated over \pt. No $\eta$ dependence is visible with the current data sample. 

\begin{figure}[h]
\vspace{-2mm}
		\centering
			\includegraphics[scale=0.35]{BFraction_fs_ETA.pdf}	\includegraphics[scale=0.35]{BFraction_fl_ETA.pdf}	
			%\vspace{-2.6cm}					
		\caption{Measurement of the fraction ratios (a) $f_s/(f_u+f_d)$ and (b) $f_{\Lb}/(f_u+f_d)$ as functions of $\eta$ integrated over \pt.
		 \label{BFraction_eta}}
\end{figure} 

%\clearpage

\subsection{\boldmath Correlation matrices for the fits to $f_s/(f_u+f_d)$ and \mbox{$f_{\Lb}/(f_u+f_d)$}}

Table~\ref{tab:ptfscor} shows the covariance matrix among the different $\pt(H_b)$ bins for $f_s/(f_u+f_d)$, while Table~\ref{tab:ptLbcor} shows the covariance matrix among the different $\pt(H_b)$ bins for $f_{\Lb}/(f_u+f_d)$.
\begin{sidewaystable}
	\caption{Covariance matrix  for $f_s/(f_u+f_d)$ in  $\pt(H_b)$~$\!$[GeV] bins; it accounts for statistical and bin-dependent systematic uncertainties, but not the global systematic uncertainties. \label{tab:ptfscor}}
	\centering
	\footnotesize
	\setlength{\tabcolsep}{3pt}
	\begin{tabular}{rccccccccccccccc}\hline\hline
$\pt(H_b)$ & 4--5&5--6&6--7&7--8&8--9& 9--10&10--11&11--12&12--13&13--14&14--16&16--18&18--20&20--25\\
\hline
4--5&2.30E-05	&3.80E-06&	1.84E-06&	 1.53E-06&	9.28E-07&	1.54E-06&	1.77E-06 &    9.05E-07&      7.47E-07&     6.99E-07&     6.46E-07&     6.84E-07 &5.47E-07	& 5.55E-07\\
5--6& 3.80E-06	&1.96E-05&	2.37E-06&      2.02E-06&	1.23E-06&	2.05E-06&	2.36E-06&	1.22E-06&	1.04E-06&	9.64E-07&	8.94E-07&	9.85E-07 & 8.04E-07	&8.00E-07 \\
6--7& 1.84E-06	&2.37E-06&	1.10E-05&	1.08E-06&	6.71E-07&	1.14E-06&	1.32E-06&	6.94E-07&	5.83E-07&	5.46E-07&	5.18E-07&	5.60E-07& 4.67E-07	&4.89E-07\\
7--8& 1.53E-06&  	2.02E-06	&1.08E-06&	1.08E-05&	6.46E-07&	1.10E-06&	1.27E-06&	6.75E-07&	5.73E-07&	5.43E-07&	5.06E-07&	5.48E-07& 4.80E-07	&4.86E-07\\
8--9 &9.28E-07&	1.23E-06&	6.71E-07	&6.46E-07&	8.73E-06&	7.75E-07&	9.09E-07&	5.07E-07&	4.26E-07&	4.04E-07&	3.79E-07&	4.16E-07&3.77E-07&3.88E-07\\
9--10&1.54E-06&	2.05E-06&	1.14E-06&	1.10E-06&	7.75E-07&	1.09E-05&	1.66E-06&	9.39E-07&	8.03E-07&	7.62E-07&	7.24E-07&	7.77E-07 & 7.01E-07& 7.34E-07\\
10--11&1.77E-06&	2.36E-06&	1.32E-06&	1.27E-06&	9.09E-07&	1.66E-06&	1.33E-05&	1.13E-06&	9.90E-07&	9.33E-07&	9.37E-07&	1.00E-06&9.03E-07	&9.58E-07\\
11--12& 9.05E-07&	1.22E-06&	6.94E-07&	6.75E-07&	5.07E-07&	9.39E-07&	1.13E-06&	1.33E-05&	5.94E-07&	5.64E-07&	5.74E-07&	6.24E-07&5.73E-07	& 6.17E-07\\
12--13& 7.47E-07&	1.04E-06&	5.83E-07&	5.73E-07&	4.26E-07&	8.03E-07&	9.90E-07&	5.94E-07&	1.60E-05&	5.42E-07&	5.62E-07&	6.33E-07&5.73E-07	&6.08E-07\\
13--14& 6.99E-07&	9.64E-07&	5.46E-07&	5.43E-07&	4.04E-07&	7.62E-07&	9.33E-07&	5.64E-07&	5.42E-07&	2.31E-05&	5.31E-07&	6.33E-07&5.98E-07&6.13E-07\\
14--16& 6.46E-07&	8.94E-07&	5.18E-07&	5.06E-07&	3.79E-07&	7.24E-07&	9.37E-07&	5.74E-07&	5.62E-07&	5.31E-07&	1.32E-05&	7.11E-07&6.66E-07	&7.27E-07\\
16--18& 6.84E-07&	9.85E-07&	5.60E-07&	5.48E-07&	4.16E-07&	7.77E-07&	1.00E-06&	6.24E-07&	6.33E-07&	6.33E-07&	7.11E-07&	1.96E-05& 8.08E-07	&9.33E-07\\
18--20& 5.47E-07&	8.04E-07&	4.67E-07&	4.80E-07&	3.77E-07&	7.01E-07&	9.03E-07&	5.73E-07&	5.73E-07&	5.98E-07&	6.66E-07&	8.08E-07&3.71E-05	&9.58E-07\\
20--25&  5.55E-07&	8.00E-07&	4.89E-07&	4.86E-07&	3.88E-07&	7.34E-07&	9.58E-07&	6.17E-07&	6.08E-07&	6.13E-07&	7.27E-07&	9.33E-07 &9.58E-07	&2.93E-05\\

%4--5&2.447E-5&4.033E-6& 1.958E-6 &1.619E-6&  9.849E-7& 1.631E-6& 1.878E-6& 9.606E-7&7.928E-7& 7.421E-7&6.856E-7&7.259E-7& 5.813E-7&5.890E-7\\
%5--6& 4.032E-6& 2.081E-5& 2.514E-6& 2.145E-6& 1.308E-6& 2.180E-6&2.507E-6&1.298E-6&1.105E-6&1.024E-6&9.494E-7&1.046E-6& 8.541E-7& 8.450E-7  \\
%6--7& 1.958E-6&2.514E-6& 1.168E-5&1.147E-6&7.122E-7& 1.212E-6&1.406E-6&7.370E-7&6.188E-7& 5.796E-7&5.496E-7&5.951E-7&4.959E-7& 5.189E-7  \\
%7--8& 1.619E-6& 2.145E-6& 1.147E-6& 1.147E-5& 6.855E-7&1.167E-6& 1.345E-6& 7.164E-7& 6.082E-7&5.766E-7&5.374E-7&5.820E-7&5.101E-7& 5.165E-7  \\
%8--9 &9.849E-7& 1.308E-6& 7.122E-7& 6.856E-7& 9.271E-6&8.224E-7&9.654E-7&5.385E-7&4.523E-7&4.291E-7& 4.027E-7&4.415E-7&4.001E-7&4.125E-7 \\
%9--10&1.631E-6& 2.181E-6&1.212E-6& 1.167E-6& 8.224E-7& 1.153E-5& 1.768E-6&9.967E-7&8.530E-7& 8.095E-7&7.683E-7& 8.248E-7&7.449E-7& 7.792E-7  \\
%10--11&1.878E-6& 2.507E-6&1.406E-6& 1.345E-6& 9.654E-7&1.768E-6& 1.410E-5&1.204E-6&1.051E-6& 9.909E-7&9.950E-7&1.061E-6&9.591E-7&1.018E-6  \\
%11--12&  9.606E-7& 1.298E-6 &7.370E-7&7.164E-7&5.385E-7& 9.966E-7&1.204E-6&1.415E-5&6.309E-7& 5.989E-7&6.091E-7& 6.630E-7&6.081E-7& 6.552E-7 \\
%12--13& 7.928E-7& 1.105E-6&6.188E-7& 6.083E-7& 4.523E-7&8.530E-7&1.051E-6& 6.309E-7&1.700E-5&5.753E-7& 5.967E-7&6.721E-7&6.084E-7& 6.454E-7  \\
%13--14& 7.421E-7& 1.024E-6&5.796E-7&5.766E-7& 4.291E-7&8.095E-7&9.909E-7& 5.989E-7& 5.753E-7&2.448E-5& 5.643E-7&6.723E-7&6.350E-7& 6.513E-7  \\
%14--16& 6.856E-7& 9.494E-7& 5.496E-7&5.374E-7&4.027E-7&7.682E-7&9.950E-7&6.091E-7& 5.967E-7& 5.643E-7& 1.396E-5& 7.546E-7&7.075E-7& 7.723E-7  \\
%16--18& 7.260E-7& 1.045E-6& 5.951E-7& 5.820E-7&4.415E-7& 8.248E-7&1.061E-6&6.629E-7& 6.721E-7&6.723E-7&7.545E-7& 2.083E-5&8.585E-7& 9.909E-7  \\
%18--20& 5.813E-7&8.541E-7& 4.959E-7&5.101E-7&4.001E-7&7.448E-7&9.591E-7&6.081E-7&6.083E-7& 6.350E-7&7.075E-7& 8.585E-7&3.939E-5&1.017E-6  \\
%20--25& 5.890E-7&8.496E-7&5.189E-7&5.165E-7&4.125E-7& 7.792E-7& 1.017E-6& 6.552E-7& 6.454E-7&6.513E-7&7.723E-7&9.909E-7&1.017E-6& 3.111E-5  \\
	\hline\hline
	\end{tabular}
\end{sidewaystable}


\begin{sidewaystable}
	\caption{Covariance matrix  for $f_{\Lb}/(f_u+f_d)$ in $\pt(H_b)$~$\!$[GeV] bins; it accounts for statistical and bin-dependent systematic uncertainties, but not the global systematic uncertainties.   \label{tab:ptLbcor}}
	\centering
	\footnotesize
	
	\setlength{\tabcolsep}{3pt}
	\begin{tabular}{rccccccccccccccc}\hline\hline
	$\pt(H_b)$ & 4--5&5--6&6--7&7--8&8--9& 9--10&10--11&11--12&12--13&13--14&14--16&16--18&18--20&20--25\\
	\hline	
		4--5&	2.40E-4	&3.21E-5	&2.08E-5	&5.03E-5	&3.36E-5	&4.21E-5	&1.60E-5	&3.50E-5	&2.47E-5	&1.30E-5	&6.20E-6&	3.10E-6	&2.60E-6	&3.46E-6\\
5--6	&3.21E-5	&4.34E-5	&5.05E-6	&1.30E-5	&8.90E-6	&1.12E-5	&4.47E-6	&9.65E-6	&7.12E-6	&3.73E-6	&1.80E-6&	9.11E-7	&7.82E-7	&1.12E-6\\
6--7	&2.08E-5&	5.05E-6	&2.99E-5	&9.39E-6	&6.55E-6	&8.41E-6	&3.35E-6	&7.43E-6	&5.44E-6	&2.86E-6	&1.43E-6&	7.04E-7	&6.25E-7	&8.96E-7\\
7--8	&5.03E-5	& 1.30E-5	&9.39E-6	&5.32E-5	&1.97E-5	&2.56E-5	&1.05E-5	&2.26E-5	&1.72E-5	    & 9.14E-6	&4.53E-6&	2.33E-6	&2.04E-6	&3.01E-6\\
8--9	&3.36E-5&	8.90E-6	&6.55E-6	&1.97E-5	&3.96E-5	&2.10E-5	&8.88E-6	&1.95E-5	&1.47E-5&	7.91E-6	&3.91E-6&	2.03E-6	&1.75E-6	&2.75E-6\\
9--10	&4.21E-5&	1.12E-5	&8.41E-6	&2.56E-5	&2.10E-5	&5.44E-5	&1.27E-5	&2.81E-5	&2.14E-5&	1.21E-5	&5.88E-6&	3.11E-6	&2.69E-6	&4.44E-6\\
10--11	&1.60E-5&	4.47E-6	&3.35E-6	&1.05E-5	&8.88E-6	&1.27E-5	&3.03E-5	&1.33E-5	&1.10E-5&	6.15E-6	&3.07E-6&	1.67E-6	&1.43E-6	&2.48E-6\\
11--12	&3.50E-5&	9.65E-6	&7.43E-6	&2.26E-5	&1.95E-5	&2.81E-5	&1.33E-5	&6.41E-5	&2.54E-5&	1.42E-5	&7.23E-6&	3.98E-6	&3.44E-6	&5.86E-6\\
12--13	&2.47E-5&	7.12E-6	&5.44E-6	&1.72E-5	&1.47E-5	&2.14E-5	&1.10E-5	&2.54E-5	&5.71E-5&	1.36E-5	&6.78E-6&	3.85E-6	&3.41E-6	&6.10E-6\\
13--14	&1.30E-5&	3.73E-6	&2.86E-6	&9.14E-6	&7.90E-6	&1.21E-5	&6.15E-6	&1.42E-5	&1.36E-5&	4.37E-5	&4.23E-6&	2.47E-6	&7.22E-6	&3.88E-6\\
14--16 &6.20E-6&	1.80E-6	&1.43E-6	&4.53E-6	&3.91E-6	&5.88E-6	&3.07E-6	&7.23E-6	&6.78E-6&	4.23E-6	&3.01E-5&	1.35E-6	&1.21E-6	&2.24E-6\\
16--18	&3.10E-6&	9.11E-7	&7.04E-7	&2.33E-6	&2.03E-6	&3.11E-6	&1.67E-6	&3.98E-6	&3.85E-6&	2.47E-6	&1.35E-6&	3.17E-5	&8.03E-7	&1.54E-6\\
18--20	&2.61E-6&	7.82E-7	&6.25E-7	&2.04E-6	&1.75E-6	&2.67E-6	&1.43E-6	&3.44E-6	&3.41E-6&	2.22E-6	&1.21E-6&	8.03E-7	&4.21E-5	&1.62E-6\\
20--25	&3.46E-6	&1.12E-6	&8.97E-7	&3.01E-6	&2.75E-6	&4.44E-6	&2.48E-6	&5.83E-6	&6.10E-6&	3.88E-6	&2.24E-6&	1.54E-6	&1.62E-6	&3.09E-5\\

%4--5&	2.401E-4	&3.207E-5	&2.077E-5	&5.029E-5	&3.357E-5	&4.206E-5	&1.596E-5	&3.496E-5	&2.473E-5	&1.303E-5	&6.201E-6&	3.096E-6	&2.601E-6	&3.463E-6\\
%5--6	&3.207E-5	&4.345E-5	&5.053E-6	&1.301E-5	&8.897E-6	&1.123E-5	&4.472E-6	&9.652E-6	&7.121E-6	&3.735E-6	&1.800E-6&	9.109E-7	&7.820E-7	&1.121E-6\\
%6--7	&2.077E-5&	5.053E-6	&2.989E-5	&9.391E-6	&6.554E-6	&8.410E-6	&3.354E-6	&7.429E-6	&5.443E-6	&2.859E-6	&1.430E-6&	7.038E-7	&6.251E-7	&8.965E-7\\
%7--8	&5.029E-5	&1.301E-5	&9.391E-6	&5.321E-5	&1.969E-5	&2.562E-5	&1.053E-5	&2.265E-5	&1.722E-5	    & 9.136E-6	&4.529E-6&	2.335E-6	&2.044E-6	&3.006E-6\\
%8--9	&3.357E-5&	8.897E-6	&6.554E-6	&1.969E-5	&3.957E-5	&2.102E-5	&8.881E-6	&1.955E-5	&1.473E-5&	7.905E-6	&3.910E-6&	2.026E-6	&1.752E-6	&2.750E-6\\
%9--10	&4.206E-5&	1.123E-5	&8.410E-6	&2.562E-5	&2.102E-5	&5.443E-5	&1.272E-5	&2.807E-5	&2.142E-5&	1.208E-5	&5.882E-6&	3.110E-6	&2.689E-6	&4.439E-6\\
%10--11	&1.596E-5&	4.472E-6	&3.354E-6	&1.053E-5	&8.881E-6	&1.272E-5	&3.027E-5	&1.333E-5	&1.100E-5&	6.146E-6	&3.068E-6&	1.674E-6	&1.434E-6	&2.476E-6\\
%11--12	&3.496E-5&	9.652E-6	&7.429E-6	&2.265E-5	&1.955E-5	&2.807E-5	&1.333E-5	&6.409E-5	&2.543E-5&	1.416E-5	&7.234E-6&	3.978E-6	&3.441E-6	&5.863E-6\\
%12--13	&2.473E-5&	7.121E-6	&5.443E-6	&1.722E-5	&1.473E-5	&2.142E-5	&1.100E-5	&2.543E-5	&5.736E-5&	1.361E-5	&6.781E-6&	3.847E-6	&3.415E-6	&6.102E-6\\
%13--14	&1.303E-5&	3.735E-6	&2.859E-6	&9.136E-6	&7.905E-6	&1.208E-5	&6.146E-6	&1.416E-5	&1.361E-5&	4.375E-5	&4.233E-6&	2.468E-6	&7.222E-6	&3.885E-6\\
%14--16 &6.201E-6&	1.800E-6	&1.430E-6	&4.529E-6	&3.910E-6	&5.882E-6	&3.068E-6	&7.234E-6	&6.781E-6&	4.233E-6	&3.010E-5&	1.348E-6	&1.206E-6	&2.242E-6\\
%16--18	&3.096E-6&	9.109E-7	&7.038E-7	&2.335E-6	&2.026E-6	&3.110E-6	&1.674E-6	&3.978E-6	&3.847E-6&	2.468E-6	&1.348E-6&	3.167E-5	&8.032E-7	&1.545E-6\\
%18--20	&2.601E-6&	7.820E-7	&6.251E-7	&2.044E-6	&1.752E-6	&2.689E-6	&1.434E-6	&3.441E-6	&3.415E-6&	2.221E-6	&1.206E-6&	8.032E-7	&4.211E-5	&1.616E-6\\
%20--25	&3.463E-6	&1.121E-6	&8.965E-7	&3.006E-6	&2.750E-6	&4.439E-6	&2.476E-6	&5.863E-6	&6.102E-6&	3.885E-6	&2.242E-6&	1.545E-6	&1.616E-6	&3.093E-5\\
	\hline\hline	

	\end{tabular}
\end{sidewaystable}




