%\pdfoutput=1
% Uncomment line above if submitting to arXiv and using pdflatex

% $Id: main.tex 124030 2018-10-12 09:08:33Z pkoppenb $
% ============================================================================
% Purpose: Template for LHCb documents
% Authors: Tomasz Skwarnicki, Roger Forty, Ulrik Egede
% Created on: 2010-09-24
% ============================================================================
\documentclass[12pt,a4paper]{article}
%%\documentclass[12pt,letter]{article}
% For two column text, add "twocolumn" as an option to the document
% class. Also uncomment the two "onecolumn" and "twocolumn" lines
% around the title page below.

% Variables that controls behaviour
\usepackage{ifthen} % for conditional statements
\newboolean{pdflatex}
\setboolean{pdflatex}{true} % False for eps figures 
\usepackage{rotating}

\newboolean{articletitles}
\setboolean{articletitles}{true} % False removes titles in references

\newboolean{uprightparticles}
\setboolean{uprightparticles}{false} %True for upright particle symbols

%\newboolean{inbibliography}
%\setboolean{inbibliography}{false} %True once you enter the bibliography
 
% Define titles and authors here. It will then be used both in metadata and in
% what is printed on the front page.
\def\paperauthors{LHCb collaboration} % Leave as is for PAPER and CONF
\def\paperasciititle{Measurement of $b$-hadron fractions in 13 TeV pp collisions} % Set ASCII title here
\def\papertitle{Measurement of $b$-hadron fractions in $13\tev$ $pp$ collisions} % Latex formatted title
\def\paperkeywords{{High Energy Physics}, {LHCb}} % Comma separated list
%\def\papercopyright{CERN on behalf of the LHCb collaboration}
\def\papercopyright{\the\year\ CERN for the benefit of the LHCb collaboration} % new since 9/Apr/2018
\def\paperlicence{CC-BY-4.0 licence}
\def\paperlicenceurl{https://creativecommons.org/licenses/by/4.0/}


\input{preamble}
\usepackage{longtable} % only for template; not usually to be used in PAPERs
\def\Lcsst{\ensuremath{\PLambda}_\cquark(2625)^+\xspace}
\def\Lcstw{\ensuremath{\PLambda}_\cquark(2765)^+\xspace}
\def\Lcstn{\ensuremath{\PLambda}_\cquark(2880)^+\xspace}
\def\Lcp{\ensuremath{\PLambda}_\cquark^+\xspace}
\def\fs{\ensuremath{f_s\xspace}}
\def\fd{\ensuremath{f_d\xspace}}
\def\fu{\ensuremath{f_u\xspace}}
%\def\flb{\ensuremath{f_\Lb\xspace}}
\def\fp{\ensuremath{f_+\xspace}}
\def\fz{\ensuremath{f_0\xspace}}
\def\flb{f_{\Lb}}
\def\rbs{f_s/(f_u+f_d)}
\def\corr{\rm corr}
\def\vcb{|\Vcb|}
\def\munu{\mu^-\overline{\nu}_{\mu}}
\DeclareRobustCommand{\myoptbar}[1]{\shortstack{{\miniscule (\rule[.5ex]{0.85em}{.18mm})}
  \\ [-.7ex] $#1$}}
\def\porpbar    {\kern 0.02em\myoptbar{\kern -0.02em p}{}\xspace}
\begin{document}



%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Title     %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\setcounter{footnote}{1}

% %%%%%%% CHOOSE TITLE PAGE--------
%\onecolumn
%\input{title-LHCb-INT}
%\input{title-LHCb-ANA}
%\input{title-LHCb-CONF}
\input{title-LHCb-PAPER}
%\twocolumn
% %%%%%%%%%%%%% ---------

\renewcommand{\thefootnote}{\arabic{footnote}}
\setcounter{footnote}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  Table of Content   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Uncomment next 2 lines if desired
%\tableofcontents
%\cleardoublepage


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Main text %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

\pagestyle{plain} % restore page numbers for the main text
\setcounter{page}{1}
\pagenumbering{arabic}

%% Uncomment during review phase. 
%% Comment before a final submission.
%\linenumbers

% You can include short sections directly in the main tex file.
% However, for larger papers it is desirable to split the text into
% several semiautonomous files, which can be revised independently.
% This is especially useful when developing a document in
% collaboration with several people, since then different parts can be
% edited independently.  This type of file organization is shown here.
% 

%\input{introduction}
Knowledge of the fragmentation fractions of \Bsb ($f_s$) and \Lb ($f_{\Lb}$) hadrons is essential for determining absolute branching fractions (${\cal{B}}$) of decays of these hadrons at the LHC, allowing measurements, for example, of ${\cal{B}}(\Bsb\to\mu^+\mu^-)$ \cite{CMS:2014xfa} and the future evaluation of $|V_{cb}|$ from $\Lb\to\Lc\mu^-\overline{\nu}_{\mu}$ decays \cite{Aaij:2017svr}.\footnote{Mention of a particular decay mode implies the use of the charge-conjugate one as well.} Once these fractions are determined, measurements of absolute branching fractions of \Bm and \Bzb mesons performed at $e^+e^-$ colliders operating at the $\PUpsilon(4S)$ resonance can be used to determine the \Bsb and \Lb branching fractions \cite{Tanabashi:2018oca}. 

In this Letter we measure the ratios  $f_s/(f_u+f_d)$ and $f_{\Lb}/(f_u+f_d)$, where the denominator is the sum of \Bm and \Bzb contributions, in the LHCb acceptance of pseudorapidity $2<\eta <5$ and transverse momentum $4< \pt<25\gev$,\footnote{We use natural units where $c=\hbar=1$.} in 13\tev $pp$ collisions. These ratios can depend on \pt and $\eta$; therefore, we perform the analysis using two-dimensional binning. 

Much of the analysis method adopted in this study is an evolution of our previous \mbox{$b$-hadron} fraction measurements for 7\tev $pp$ collisions\cite{Aaij:2011jp}.  We use the inclusive semileptonic decays $H_b\to H_c X\mu^-\overline{\nu}_{\mu}$, where $H_b$ indicates a $b$ hadron, $H_c$ a charm hadron, and $X$  possible additional particles. Each of the different $H_c$ plus muon final states can
originate from the decay of different $b$ hadrons. Semileptonic decays of $\Bzb$ mesons usually result in a mixture of $D^0$ and $D^+$ mesons, while $B^-$ mesons decay predominantly into $D^0$ mesons with a smaller admixture of $D^+$ mesons. Both include a tiny component of $\Ds \Kb$ meson pairs. Similarly, $\Bsb$ mesons decay predominantly into $D_s^+$ mesons, but can also decay into $D^0K^+$ and $D^+\Kz$ meson pairs; this is expected if the 
$\Bsb$ meson decays into an excited $D_s^+$ state that is heavy enough to decay into a $DK$ pair. We measure this contribution using $D^0K^+X\mu^-\overline{\nu}_{\mu}$ events.  Finally, $\Lb$ baryons decay semileptonically mostly into $\Lc$ final states, but can also decay into $D^0p$ and $D^+n$ pairs. We ignore the contributions of $b\to u$ decays that comprise  approximately 1\% of semileptonic $b$-hadron decays, and contribute almost equally to all $b$-hadron species. The detailed equations relating these yields to the final results are given in Ref.~\cite{Aaij:2011jp} and in the Supplemental material. 

The theoretical basis for this measurement is the near equality of semileptonic widths, $\Gamma_{\rm SL}$, for all $b$-hadron species \cite{Bigi:2011gf} whose differences are predicted to precisions of about 1\%. The values we use for the individual $H_b$ semileptonic branching fractions (${\cal{B}}_{\rm SL}$) are listed in Table~\ref{tab:slpar}. The $H_c$ decay modes used and their branching fractions are given in Table~\ref{tab:charmBF}.

The ratio of \Dp to \Dz meson production in the sum of semileptonic \Bzb and \Bm decays, 
${\fp}/{\fz}$,  is used to check the analysis method. This result can be related to models of the hadronic final states in $\Bm$ and $\Bzb$ semileptonic decays \cite{Rudolph:2018rzl}. 


\input{detector}
%%%
\begin{table}[t]
\begin{center}
\caption{Branching fractions of semileptonic $b$-hadron decays from direct measurements for \Bzb and \Bm mesons and derived  for $\Bsb$ and \Lb hadrons based on the equality of semileptonic widths and the lifetime ratios~\cite{Tanabashi:2018oca,Bigi:2011gf}. Corrections to $\Gamma_{\rm SL}$ are also listed. Correlations in the \Bzb and \Bm branching fraction measurements have been taken into account. See Ref.~\cite{Aaij:2016avz} for more information.}
\vskip 3mm
\label{tab:slpar}
%\begin{footnotesize}
%\def\arraystretch{1.1}
\begin{tabular}{lcccc}
\hline\hline
Particle  &   $\tau$ (ps) & ${\cal{B}}_{\rm SL}$  (\%) & Correction (\%) & ${\cal{B}_{\rm SL}}$ (\%)  \\
                &  measured     & measured                  &          to $\Gamma_{\rm SL}$     \cite{Bigi:2011gf}              & used \\
\hline 
\rule{0pt}{12pt}$\Bzb$ & $1.520\pm 0.004$ & $10.30\pm 0.19$&  ~&$10.30\pm 0.19$  \\
$B^-$ & $1.638\pm 0.004$ & $11.08\pm 0.20$& ~&  $11.08\pm 0.20$  \\
$\left< \Bzb +B^-\right>$&&$10.70\pm 0.19$ &&  $10.70\pm 0.19$  \\
$\Bsb$ & $1.526 \pm 0.015$ &  &--1.0$\pm$0.5& $10.24\pm 0.21$    \\
$\Lb$ & $1.470\pm 0.010$ & &~3.0$\pm$1.5 &  $10.26\pm 0.25$   \\
\hline\hline
\end{tabular}
%\end{footnotesize}
%\def\arraystretch{1.0}
\end{center}
\end{table}
\begin{table}[b]
\begin{center}
	\caption{Charm-hadron branching fractions for the decay modes used in this analysis. 
	%$D^{0}\rightarrow  K^-\pi^+$ and $D_s^+\to K^-K^+\pi^+$  are taken from Ref.~\cite{Tanabashi:2018oca}. The $\Lambda_{c}^{+}\to pK^-\pi^+$ fraction is a weighted average of direct measurements by BES III\cite{Ablikim:2015flg} and Belle \cite{Zupanc:2013iki}. $D^+\to K^-\pi^+\pi^+$ is taken from Ref.~\cite{PhysRevD.89.072002}
	}
	\vskip 3mm
	\label{tab:charmBF}
	\begin{tabular}{lcl}
	\hline\hline
	Decay &  ${\cal{B}}$ (\%)  &~~ ~~~~~Source \\
	\hline
	$D^0\to K^-\pi^+$ 			& $3.93\pm 0.05$  & PDG average \cite{Tanabashi:2018oca} \\
	$D^+\to K^-\pi^+\pi^+$ 		& $9.22\pm 0.17$  & CLEO III \cite{Bonvicini:2013vxi}\\
	$D_s^+\to K^-K^+\pi^+$ 		& $5.44\pm 0.18$  &PDG average \cite{Tanabashi:2018oca} \\	$\Lc\to pK^-\pi^+$   & $6.23\pm 0.33$    &Weighted average of Belle \\
	~~&~~& and BES III results \cite{Ablikim:2015flg,Zupanc:2013iki}\\\hline\hline
\end{tabular}
\end{center}
\end{table}


Previous studies at 7\tev \cite{Aaij:2011jp,Aaij:2013qqa,Aaij:2014jyk} have shown that $f_s/(f_u+f_d)$ and $f_{\Lb}/(f_u+f_d)$ depend upon the kinematics of the decaying $b$ hadron. In particular, there is a strong dependence upon the transverse momentum of the $\Lb$ baryon. We investigate this phenomenon at 13\tev both for \Bsb and \Lb fractions.

Selection criteria are applied to muons and $H_c$ decay particles. The transverse momentum of each hadron must be greater than 0.3\gev, and that of the muon larger than 1.3\gev.  Each track cannot point to any PV, implemented by requiring
$\chisqip >9$ with respect to any PV, where \chisqip\ is defined as the difference in the vertex-fit \chisq of a given PV reconstructed with and
without the track under consideration being included. All final state particles are required to be positively identified using information from the RICH detectors (PID). Particles from $H_c$ decay candidates must have a good fit to a common vertex with $\chi^2$/ndof $<9$, where ndof is the number of degrees of freedom. They must also be well separated from the nearest PV, with the flight distance divided by its uncertainty greater than 5. 

Candidate $b$ hadrons are formed by combining $H_c$ and muon candidates originating from a common vertex with $\chi^2$/ndof $<9$ and an $H_c\mu^-$ invariant mass, $m_{H_c\mu^-}$, in the range 3.0--5.0\gev for $D^0$ and $D^+$, 3.1--5.1\gev for \Ds and 3.3--5.3\gev for \Lc candidates. In addition, we define
$m_{\rm corr} \equiv \sqrt{m_{H_{c}\mu}^{2} + p_{\perp}^{2}} + p_{\perp}$, where $p_{\perp}$ is the magnitude of the combination's momentum component transverse to the $b$-hadron flight direction; we require that $m_{\rm corr}>4.2$ or $4.5\gev$ for \Bsb or \Lb candidates, respectively.
For the $\Ds\to K^+K^-\pi^+$ decay mode, vetoes are employed to remove backgrounds from real $D^+$ or \Lc decays where the particle assignments are incorrect.

Background from prompt $H_c$ production at the PV needs to be considered. We use the natural logarithm of the $H_c$ impact parameter, IP, with respect to the PV in units of mm.
 Requiring  ln(IP/mm)$>-3$ is found to reduce the prompt component to be below 0.1\%,  while preserving 97\% of all signals. This restriction allows us to perform fits only to the $H_c$ candidate mass spectra to find the $b$-hadron decay yields.


\begin{figure}[t]
\vskip -2mm
		\centering
			\includegraphics[scale=0.33]{D0_Mass_Pt_0_Eta_0.pdf}\includegraphics[scale=0.33]{Dp_Mass_Pt_0_Eta_0.pdf}			                   \includegraphics[scale=0.33]{Ds_Mass_Pt_0_Eta_0.pdf}\includegraphics[scale=0.33]{Lc_Mass_Pt_0_Eta_0.pdf}
		\caption{Fit to the mass spectra of the $H_c$ candidates of the selected $H_b$ decays: (a) $D^0$, (b) $D^+$, (c) $D_s^+$ mesons, and (d) the \Lc baryon.  The data are shown as black points with error bars. The signal component is shown as the dashed (green) line and the combinatorial background component is shown as the dashed (red) line. The solid (blue) line shows all components added together.\label{fig:charm-mass}}
\end{figure}

The $H_c$ candidates mass distributions integrated over $\pt(H_b)$ and $\eta$ are shown in Fig.~\ref{fig:charm-mass} and consist of a prominent peak resulting from signal, and a small contribution due to combinatorial background from random combinations of particles that pass the selection. They are fit with a signal component comprised of two Gaussian functions, and a combinatorial background component modeled as a linear function. The total signal yields for $D^0 X\mu^-\overline{\nu}_{\mu}$, $D^+ X\mu^-\overline{\nu}_{\mu}$, $D_s^+ X\mu^-\overline{\nu}_{\mu}$ and $\Lc\mu^-X\overline{\nu}_{\mu}$ are 13~$\!$775~$\!$000, 4~$\!$282~$\!$700,  845~$\!$300, and 1~$\!$753~$\!$600, respectively.

Background contributions to the $b$-hadron candidates include hadrons faking muons, false combinations of charm hadrons and muons from the two $b$ hadrons in the event, as well as real muons and charm hadrons from $B\to \D\Db X$ decays, where one of the $D$ mesons decays into a muon. All the backgrounds are evaluated in two-dimensional $\eta$ and $\pt$ intervals. The first two backgrounds are evaluated using events where the  $H_c$ is combined with a muon of the wrong-sign ({\it e.g.} $D^0\mu^+$), forbidden in a semileptonic $b$-hadron decay. The wrong-sign backgrounds are $<1$\% for each $H_c$ species. The background from $B\to D\Db X$ decays is determined by simulating a mixture of these decays using their measured branching fractions \cite{Tanabashi:2018oca}. The only decay mode significantly affected is $\Bsb\to \Ds X\mu^-\overline{\nu}_{\mu}$ with contributions varying from 0.1\% for $D^0D_s^- X$  to 1.8\% for $\Ds D_s^- X$ due to the large $\Ds\to\mu^+\nu$ decay rate. The total $B\to D\Db X$ background is $(5.8\pm 0.9)\%$.

The dominant component in \Bsb semileptonic decays is $\Ds X\mu^-\overline{\nu}_{\mu}$, where $X$ contains possible additional hadrons. However, the \Bsb meson also can decay into $D^0 K^+$ or $D^+K^0$ instead of  \Ds, so we must add this component to the \Bsb rate and subtract it from the $f_u+f_d$ fraction. 
Similarly, in \Lb semileptonic decays we find a $D^0pX$ component. The selection criteria for these final states are similar to those for the $D^0 X\mu^-\overline{\nu}_{\mu}$ and $\Lc X\mu^-\overline{\nu}_{\mu}$ final states described above with the addition of a kaon or proton with $\pt>300\mev$ that has been positively identified. A veto is also applied to reject $D^{*+}\to\pi^+D^0$ decays where the pion mimics a kaon or a proton.

These samples contain background, resonant and nonresonant decays. Separation of these components is achieved by using both right-sign ($H_c$ with $\mu^-$) and wrong-sign ($H_c$ with $\mu^+$) candidates.  In addition, the logarithm of the difference between the vertex $\chi^2$ formed by the added hadron track and the $D\mu$ system and the  vertex $\chi^2$  of  the $D\mu$ system, ${\rm ln}(\Delta\chi^{2}_{\rm V})$, provides separation between combinatorial background and nonresonant semileptonic decays.  True resonant and nonresonant $\Bsb\to\Dz\Kp\mu^-\overline{\nu}_{\mu}$ or $\Lb\to\Dz p\mu^- \overline{\nu}_{\mu}$ decays peak in the ${\rm ln}(\Delta\chi^{2}_{\rm V})$ distribution at a value of unity while the background is smooth and rises at higher values as the added track is generally not associated with the $\Dz\mun$ vertex. To distinguish signal from background we define $m(D^{0}h)_C\equiv m(D^0h)-m(D^0)+m(D^0)_{\rm PDG}$, and perform two-dimensional fits to the $m(D^{0}h)_C$ and ${\rm ln}(\Delta\chi^{2}_{\rm V})$ distributions, where $h=K^+(p)$ for right-sign \Bsb  (\Lb) decays. 
%Therefore, two-dimensional fits to the $m(D^{0}h)_C$ and ${\rm ln}(\Delta\chi^{2}_{\rm V})$ distributions are implemented where $h$ is a $K^+$ for right-sign \Bsb decays and a $p$ for right-sign \Lb decays. 

\begin{figure}[t]
\vspace{-2mm}
\begin{center}
\includegraphics[width=0.4\textwidth]{reduced_ntuple_2016combined_rs_mDK_full_range_2dfit_result}
\includegraphics[width=0.4\textwidth]{reduced_ntuple_2016combined_rs_ipchi2_full_range_2dfit_result}\\
\includegraphics[width=0.4\textwidth]{reduced_ntuple_2016combined_rs_mDp_full_range_2dfit_result_l1_forpaper}
\includegraphics[width=0.4\textwidth]{reduced_ntuple_2016combined_rs_ipchi2_full_range_2dfit_result_l1_forpaper}
\caption{ Projections of the two-dimensional fits to the (a)  $m(D^0K^{\pm})_C$ and (c) $m(D^0\porpbar)_C$ mass distributions and (b, d) ${\rm ln}(\Delta\chi^{2}_{\rm V})$ 
for (top) $\Dz K^{\pm} X\mu^- \overline{\nu}_{\mu}$ candidates, and (bottom) for $\Dz \porpbar X\overline{\nu}_{\mu}$ candidates.
The curves show projections of the 2D fit. The  dashed (red) curves show the 
$D_{s1}^{+}$ and $D_{s2}^{*+}$ resonant components in (a) and (b), and $\Lc(2860)$, $\Lc(2880)$ and $\Lc(2940)$
resonant components in (c) and (d). The long-dashed-dot (green) curves show the nonresonant component, the
dotted (black) curves are the background components, whose shapes are determined from wrong-sign combinations, and the solid (blue) curve shows all components added together.}
\label{fig:full_fits_bs}
\end{center}
\end{figure}
The wrong-sign shapes are used to model the backgrounds.  The resonant structures are modeled with relativistic Breit--Wigner functions convoluted with Gaussians to take into account the experimental resolution, except for the $D_{s1}(2536)^+$ which is modeled with the sum of two Gaussians with a fixed mean. The nonresonant shape for the ${\rm ln}(\Delta\chi^{2}_{\rm V})$ distribution is taken as the same as the resonant one.  Figure~\ref{fig:full_fits_bs} shows the data and result of the fits for \Bsb and \Lb candidates. 

For the \Bsb case, we find $22~\!610\pm210$ $D_{s1}(2536)^+$, $14~\!290\pm260$ $D_{s2}^*(2573)^{+}$, and $38~\!140\pm460$ nonresonant decays, confirming the existence of both the $D_{s1}^+$ \cite{Abazov:2007wg,Aaij:2011ju} and $D_{s2}^{*+}$ \cite{Aaij:2011ju} particles in semileptonic \Bsb decays with substantially more data, and showing the existence of the nonresonant component. To account for the unmeasured $D^+K^0$ channel we take different mixtures of $D^*$ and $D$ final states for the different resonant and nonresonant components. The $D_{s1}^+$ decays dominantly into $D^*$, while the $D_{s2}^{*+}$ decays dominantly into $D$ mesons \cite{Tanabashi:2018oca}. For the nonresonant part we assume equal $D^*$ and $D$ yields. 

In the \Lb case, we find $6120\pm460$ $\Lc(2860)$, $2200\pm200$ $\Lc(2880)$, $1200\pm260$ $\Lc(2940)$, and $29~\!770\pm690$ nonresonant events. The decay rate into $D^0p$ is assumed to be equal to that into $D^+n$ using isospin conservation. All decays with an extra hadron have lower detection efficiencies than the sample without. 


Efficiencies for all the samples are determined using data in two-dimensional $\pt$ and $\eta$ bins.
Trigger efficiencies are determined using a sample of \mbox{$B^-\to\jpsi K^-$,} with $\jpsi\to\mup\mun$ decays where only one muon track is positively identified, in conjunction with viewing the effects of combinations of different triggers 
\cite{LHCb-PUB-2014-039}. This sample is also used to determine muon identification efficiencies.
Decays of \jpsi mesons to muons reconstructed using partial information from the tracking system,  {\it e.g.} eliminating the vertex locator information, are also used to determine tracking efficiencies using data and to correct the simulation.
 Finally, the PID efficiencies are evaluated using 
%selected samples of known particles obtained without and identification requirements, 
kaons and pions from $D^{*+}\to\pi^+D^0$ decays, with $D^0\to K^-\pi^+$, and protons from $\PLambda\to p \pi^-$ and $\Lc\to p K^-\pi^+$ decays \cite{Aaij:2018vrk}. In the measurement of $b$-hadron fraction ratios many of the efficiencies cancel and we are left with only residual effects to which we assign systematic uncertainties.

The $b$-hadron $\eta$ and \pt, $\pt(H_b)$, must be known because the $b$ fractions can depend on production kinematics.
While $\eta$ can be evaluated directly using the measured primary and secondary $b$ vertices, the value of $\pt(H_b)$  must be determined to account for the missing neutrino plus extra particles. The correction factor $k$ is given by the ratio of the average reconstructed to true $\pt(H_b)$ as a function of $m(H_c\mu^-)$ and is determined using simulation. It varies from 0.75 for $m(H_c\mu^-)$ equals 3\gev to 1 at the nominal $b$-hadron mass for all hadron species. 


\begin{figure}[b]
%\vspace{-2.5cm}
		\centering
			\includegraphics[scale=0.5]{BFraction_fs_1DPT_Fit_KFact.pdf}	
%			\vspace{-2cm}
					\caption{Measured $f_{s}/(f_{u}+f_{d})$ ratio in bins of $\pt(H_b)$. The smaller (black)  error bars show the combined bin-by-bin statistical and systematic uncertainties, and the larger (blue) ones show the global systematics added in quadrature. The fit to the data is shown as the solid (green) band, whose width represents the $\pm 1 \sigma$ uncertainty limits on the fit shape, and the  dashed (black) lines gives the total uncertainty on the fit result including the global scale uncertainty.  \label{fig:fs_fit_k}}
\end{figure}


The distribution of $f_{s}/(f_{u}+f_{d})$ as a function of $\pt(H_b)$ is shown in Fig.~\ref{fig:fs_fit_k}. We perform a linear $\chi^{2}$ fit incorporating a full covariance matrix which takes into account the bin-by-bin correlations introduced from the kaon kinematics, and PID and tracking systematic uncertainties. The factor $A$ in Eq.~\ref{eq:fs} incorporates the global systematic uncertainties described later, which are independent of  $\pt(H_b)$. The resulting function is

\begin{equation}
\label{eq:fs}
\frac{f_{s}}{f_{u}+f_{d}}(\pt) = A\left[p_1 + p_2 \times \left(\pt-\langle\pt\rangle\right)\right],
\end{equation}
where \pt here refers to $\pt(H_b)$, $A=1\pm 0.043$, $p_1=0.119 \pm 0.001$, $p_2=(-0.91 \pm 0.25) \cdot 10^{-3}~\!{\rm GeV}^{-1}$, and  $\langle\pt\rangle=10.1\gev$.
The correlation coefficient between the fit parameters is 0.20. After integrating over $\pt(H_b)$, no $\eta$ dependence is observed (see the Supplemental material).


 We determine an average value for $f_{s}/(f_{u}+f_{d})$ by dividing the yields of $\Bsb$ semileptonic decays by the  sum of $\Bzb$ and $\Bm$ semileptonic yields, which are all efficiency-corrected, between the limits of $\pt(H_b)$ of 4 and 25\gev and $\eta$ of 2 and 5, resulting in
\begin{equation}
\frac{f_{s}}{f_{u}+f_{d}} = 0.122 \pm 0.006,\nonumber
\end{equation}
where the uncertainty contains both statistical and systematic components, with the latter being dominant, and discussed subsequently. The total relative uncertainty is 4.8\%. 

\begin{figure}[b]
%\vspace{-2cm}
		\centering
			\includegraphics[scale=0.5]{BFraction_fl_1DPT_Fit_KFact.pdf}	
%			\vspace{-2.cm}
		\caption{Measured $f_{\Lb}/(f_{u}+f_{d})$ ratio in bins of $\pt(H_b)$. The description is the same as in Fig.~\ref{fig:fs_fit_k}.
		%		The smaller error bars (black) show the combined bin-by-bin statistical and systematic uncertainties, and the larger (blue) error bars show the global systematics added in quadrature. The fit to the data is shown as the solid (green) curve, whose width represents the $\pm 1 \sigma$ uncertainty limits on the fit shape, and the dashed black curves give the total uncertainty on the fit result including the global scale uncertainty.
		 \label{fig:fl_fit_kfact}}
\end{figure}

Figure~\ref{fig:fl_fit_kfact} shows the \Lb fraction as a function of $\pt(H_b)$ demonstrating a large \pt dependence. The distribution in $\eta$ is flat. We perform a similar fit as in the \Bsb fraction case, finding
\begin{equation}
\label{eq:fLb}
\frac{f_{\Lb}} {f_{u}+f_{d}}(\pt) =A\left[ p_1 + \exp\left( p_2 +p_3 \times \pt\right)\right],\\
\end{equation}
where \pt here refers to $\pt(H_b)$, $A=1\pm 0.061$, $p_1=( 7.93\pm 1.41)\cdot10^{-2}$, $p_2=-1.022 \pm 0.047$, and \mbox{$p_3= -0.107 \pm 0.002~\!{\rm GeV}^{-1}$}.
The correlation coefficients among the fit parameters are 0.40 $(\rho_{12})$, --0.95 $(\rho_{13})$, and --0.63 $(\rho_{23})$.
%\begin{equation}
%C = 	\begin{pmatrix}
%	1 &0.37 & -0.96\\
%	 0.37  & 1 &  -0.56\\
%	  -0.96 & -0.56 & 1\\
%	\end{pmatrix}
%\end{equation}




The average value for $f_{\Lb} / (f_{u}+f_{d})$ is determined using the same method as in the \Bsb case.  The result is
\begin{equation}
\frac{f_{\Lb}}{f_{u}+f_{d}} = 0.259  \pm 0.018,\nonumber
\end{equation}
where the dominant uncertainty is systematic, and the statistical uncertainty is included. The overall uncertainty is 6.9\%. 

As a systematic check of the analysis method, and a useful measurement to test the knowledge of known semileptonic branching fractions and extrapolations used to saturate the unknown portion of the inclusive hadron spectrum, we measure the ratio of the $D^0 X\mu^-\overline{\nu}_{\mu}$ to $D^+ X\mu^-\overline{\nu}_{\mu}$ corrected yields $f_{+} / f_{0}$. We subtract the small contributions  from \Bsb and \Lb decays, and a very small contribution from $B\to \Ds \Kb \mu^- X$ decays has been taken into account \cite{delAmoSanchez:2010pa}, as in all the fractions measured above.

Assuming $f_{u}$ equals $f_{d}$, Ref.~\cite{Rudolph:2018rzl} estimates the fraction of $D^{+}\mu$ with respect to $D^{0}\mu$ modes in the sum of $\Bm$ and $\Bzb$ decays as $0.387\pm0.012\pm0.026$. The first uncertainty comes from the uncertainties on known measurements. The second uncertainty comes from the different extrapolations from excited $D$ mesons used to saturate the remaining portion of the inclusive rate.
%, thus this uncertainty is an envelope of all possible solutions for $f_{+} / f_{0}$ depending on which extrapolation is used. 

The $f_{+} / f_{0}$  ratio must be independent of $\eta$ and \pt. 
To derive an overall value for $f_{+} / f_{0}$, the $\pt(H_b)$ distribution is fit to a constant.  Only the PID and tracking systematic uncertainties on the second pion in the $D^{+}$ decay need be considered. 
%These uncertainties are also correlated between bins as the pion kinematics are similar in adjacent $\pt$ bins. 
Performing a $\chi^{2}$ fit using the full covariance matrix we find 
\begin{equation}
 f_{+} / f_{0}=0.359\pm0.006\pm 0.009,\nonumber
\end{equation}
where the first uncertainty is from bin-by-bin statistical and systematic uncertainties, including correlations, and the second is systematic. The $\chi^2$/ndof is 0.63, in agreement with a flat spectrum. The measurement is consistent with the prediction and places some constraints on the $D^{**}$ content of  semileptonic $B$ decays \cite{Rudolph:2018rzl}.

  The dominant global systematic uncertainties are listed in Table~\ref{tab:Systematics}. Simulation uncertainties are due to the modelling of excited charm states for the $f_s/(f_u+f_d)$ determination and the weighting required for the $f_{\Lb}/(f_u+f_d)$ ratio, due to differences between the simulated and measured \pt spectra. Background uncertainties arise from $D\Db X$ final states with uncertain branching fractions. Cross-feed uncertainties come from errors on efficiency estimates and the assumed $D^*$ to $D$ mixtures.
  Other smaller uncertainties depend on $\pt(H_b)$ and include tracking (0.2--1.8)\%, particle identification (0.4--3.0)\%, trigger (0.3--3.9)\% and $k$-factor (0.2--1.8)\%.

\begin{table}[b]
	\caption{Global systematic uncertainties. The $D^{0}$ and $D^{+}$ branching fraction uncertainties are scaled by the fraction of each decay, $f_0$ and $f_+$ for $f_s/(f_u+f_d)$ and  $f_{\Lb}/(f_u+f_d)$ uncertainties.
	 \label{tab:Systematics}}
	\centering
	\begin{tabular}{lccc}
	 \hline\hline
	Source &  \multicolumn{3}{c}{Value (\%)} \\
	             & $f_s/(f_u+f_d)$ &   $f_{\Lb}/(f_u+f_d)$ & $f_+/f_0$ \\ [0.1cm]
	\hline	
	Simulation 							&  1.7      & 2.4		&--\\
	Backgrounds 							&  0.9       & 0.3		&--\\
	Cross-feeds							&  1.2       & 0.4		& 0.2\\
 	$\mathcal{B}$($D^{0} \rightarrow K^-\pi^+$)	&  1.0 	&1.0		& 1.3  \\
	$\mathcal{B}$($D^{+} \rightarrow K^+\pi^-\pi^-$) & 0.6 	& 0.6		& 1.8\\
	$\mathcal{B}$($D^{+}_{s} \rightarrow K^+K^-\pi^+$) & 3.3 &      --  	&-- \\
	$\mathcal{B}$($\Lc \rightarrow pK^+\pi^-$) 		& --   		& 5.3&-- \\
	Measured lifetime ratio					& 1.2 	& 0.7  	& --\\
	$\Gamma_{sl}$ correction					& 0.5   	& 1.5	 	&-- \\
	\hline
	Total  								& 4.3		& 6.1	        &2.2\\
	\hline\hline
	\end{tabular}
	
\end{table}

In conclusion, we measure the ratios of \Bsb and \Lb production to the sum of \Bm and \Bzb to be $\pt(H_b)$ dependent
(see Eqs.~\ref{eq:fs} and \ref{eq:fLb}).
The averages in the ranges $4<\pt(H_b)<25\gev$, and $5<\eta< 2$ are \mbox{$f_s/(f_u+f_d)=0.122 \pm 0.006$},  and \mbox{$f_{\Lb}/(f_u+f_d)=0.259  \pm 0.018$}, respectively. Using 7\tev data, LHCb determined $f_{s}/(f_u+f_d)=0.1295  \pm 0.0075$ with a $\pt(H_b)$ slope larger than, but consistent, with these 13\tev results \cite{Aaij:2013qqa}; no dependence on $\eta$ was observed.  For the $\Lb$ baryon, the fraction ratio is consistent with the 7\tev measurements after taking into account the different $\pt(H_b)$ ranges used \cite{Aaij:2014jyk,Aaij:2015fea,Aaij:2011jp}. We observe no rapidity dependence over a similar $\pt(H_b)$ range as in Ref.~\cite{Aaij:2015fea}.


These results are crucial for determining absolute branching fractions of \Bsb and \Lb hadron decays in LHC experiments. 
We also determine the ratio of $D^0$ to $D^+$ mesons produced in the sum of  $\Bzb$ and \Bm semileptonic decays as \mbox{$f_{+} / f_{0}=0.359\pm0.006\pm 0.009$}.
% Do not include this in any draft (just for information in the template)
%\input{acknowledgements_intro}
% Comment this in for paper drafts; do not include this in analysis note and conference reports
%\input{acknowledgements}
\clearpage
\newpage
\input{Supplementary}

%\input{appendix}

% This should be taken out in the final paper
%\input{supplementary-app}
\newpage
\addcontentsline{toc}{section}{References}
%\setboolean{inbibliography}{true}
\bibliographystyle{LHCb}
\bibliography{bfrac-13,LHCb-DP,LHCb-PAPER}
 

\newpage
\input{LHCb_Authorship_11-Dec-2018.tex}



\end{document}



